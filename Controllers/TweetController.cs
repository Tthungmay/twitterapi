using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TwitterApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TwitterApi.Controllers
{
    [Route("api/[controller]")]
    public class TweetController : Controller
    {
        public class MyUser
        {
            public string sessionID { get; set; }
            public string userID { get; set; }
        }
        public ITweetRepository _tweets { get; set; }
        private static HttpClient client = new HttpClient();

        public TweetController(ITweetRepository tweets)
        {
            _tweets = tweets;
        }

        protected string GetUserIdFromSession(string sessionid)
        {
            HttpResponseMessage response = client.GetAsync("http://localhost:5000/api/session/" + sessionid).Result;
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                MyUser user = JsonConvert.DeserializeObject<MyUser>(result);
                return user.userID;
            }
            else
            {
                return null;
            }
        }
        
        [HttpGet]
        public IEnumerable<Tweet> Get(DateTime recent, string sessionid)
        {
            if(sessionid != null && sessionid != "" && recent != null) {
                return _tweets.GetAllUserRecent(GetUserIdFromSession(sessionid), recent);
            } else if(recent != null) {
                return _tweets.GetAllRecent(recent);
            }
            return _tweets.GetAll();
        }

        [HttpGet("{id}")]
        public Tweet Get(int id)
        {
            return _tweets.Find(id);
        }

        // Your work here, this function should return list of tweet from user's followings
        [HttpGet("timeline")]
        public IEnumerable<Tweet> GetTimeline(DateTime recent, string sessionid)
        {
            //Hint:
            // 1. get all followings.
            // 2. for each following, get their recent tweets.
            // 3. combine them, return.
            List<Following> followings = new List<Following>();
            HttpResponseMessage response = client.GetAsync("http://localhost:5200/api/following/?sessionid=" + sessionid).Result;
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                followings = JsonConvert.DeserializeObject<List<Following>>(result);
            }
            List<Tweet> list_tweet = new List<Tweet>();
            if (sessionid != null && sessionid != "" && recent != null)
            {
                foreach (Following f in followings.ToList())
                {
                    foreach (Tweet t in _tweets.GetAllUserRecent(Convert.ToString(f.Follow), recent))
                    {
                        list_tweet.Add(t);
                    }
                }
                foreach (Tweet t in _tweets.GetAllUserRecent(GetUserIdFromSession(sessionid), recent))
                {
                    list_tweet.Add(t);
                }
                return list_tweet;
            }
            else if (recent != null)
            {
                return _tweets.GetAllRecent(recent);
            }
            return null;
        }
        

        [HttpPost]
        public IActionResult Create([FromBody]Tweet tweet, string sessionid)
        {
            if (sessionid == null || sessionid == "")
            {
                return BadRequest();
            }
            tweet.Owner = Int32.Parse(GetUserIdFromSession(sessionid));
            _tweets.Add(tweet);
            return Ok();
        }
    }
}
