using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwitterApi.Models
{
    public class Following
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FollowingID { get; set; }
        public int User { get ; set;}
        public int Follow { get; set;}
    }
}